﻿using Insurance.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insurance.AuthenticationService
{
    public class AuthenticationService : BaseAuthenticationService
    {

        public AuthenticationService(IRepository repository)
        {
            _repository = repository;
        }

        public override bool AuthorizeAgent(string login, string password)
        {
            var agent = _repository.GetAgentLoginData(login);
            if (agent != null && password.Equals(agent.Password))
            {
                _repository.AuthorizedAgent = agent.Agent;
                return true;
            }

            return false;
        }

        public override bool AuthorizeDirector(string login, string password)
        {
            var director = _repository.GetDirectorByLogin(login);
            if ( director != null && password.Equals(director.Password))
            {
                _repository.AuthorizedDirector = director;
                return true;
            }

            return false;
        }

    }
}
