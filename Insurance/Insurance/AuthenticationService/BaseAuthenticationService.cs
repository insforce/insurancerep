﻿using Insurance.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insurance.AuthenticationService
{
    public abstract class BaseAuthenticationService : IAuthenticationService
    {
        protected IRepository _repository;
        public abstract bool AuthorizeAgent(string login, string password);
        public abstract bool AuthorizeDirector(string login, string password);
    }
}
