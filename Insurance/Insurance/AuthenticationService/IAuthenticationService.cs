﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insurance.AuthenticationService
{
    public interface IAuthenticationService
    {
        bool AuthorizeDirector(string login, string password);

        bool AuthorizeAgent(string login, string password);

    }

}
