﻿﻿using Insurance.DataAccess;
using Insurance.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Insurance.AuthenticationService;
using Insurance.ViewModel;


namespace Insurance.DIModules
{
    class IocConfiguration : NinjectModule
    {
        public override void Load()
        {

            Bind<IRepository>().To<Repository>().InSingletonScope(); // Reuse same storage every time


            Bind<IAuthenticationService>().
                To<AuthenticationService.AuthenticationService>().InSingletonScope();

            Bind<AgentViewModel>().ToSelf().InTransientScope(); // Create new instance every time

            Bind<AgentLoginViewModel>().ToSelf().InTransientScope();

            Bind<DirectorLoginViewModel>().ToSelf().InTransientScope();

            Bind<LoginViewModel>().ToSelf().InTransientScope();

        }
    }
}
