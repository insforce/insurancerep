﻿using Insurance.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insurance.DIModules
{
    public class ViewModelLocator
    {
        public AgentViewModel AgentViewModel
        {
            get { return IocKernel.Get<AgentViewModel>(); }
        }

        public LoginViewModel LoginViewModel
        {
            get { return IocKernel.Get<LoginViewModel>(); }
        }

        public AgentLoginViewModel AgentLoginViewModel
        {
            get { return IocKernel.Get<AgentLoginViewModel>(); }
        }

        public DirectorLoginViewModel DirectorLoginViewModel
        {
            get { return IocKernel.Get<DirectorLoginViewModel>(); }
        }
    }
}
