﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class AgentMap : ObservableObject
    {
        #region Members
        private Agent _agent;
        #endregion

        #region Constructors
        public AgentMap()
        {
            _agent = new Agent { FIO = "Unknown", Address = "Unknown", Telephone = "Unknown", BranchId=1 };
        }

        public AgentMap(Agent agent)
        {
            this._agent = agent;
        }
        #endregion

        #region Properties
        public Agent Agent
        {
            get
            {
                return _agent;
            }
            set
            {
                _agent = value;
            }
        }

        public string FIO
        {
            get { return Agent.FIO; }
            set
            {
                if (Agent.FIO != value)
                {
                    Agent.FIO = value;
                    RaisePropertyChanged("FIO");
                }
            }
        }

        public string Address
        {
            get { return Agent.Address; }
            set
            {
                if (Agent.Address != value)
                {
                    Agent.Address = value;
                    RaisePropertyChanged("Address");
                }
            }
        }

        public string Telephone
        {
            get { return Agent.Telephone; }
            set
            {
                if (Agent.Telephone != value)
                {
                    Agent.Telephone = value;
                    RaisePropertyChanged("Telephone");
                }
            }
        }

        public BranchMap Branch
        {
            get 
            {
                return new BranchMap(_agent.Branch);
            }
            set
            {
                if (Branch != value)
                {
                    _agent.Branch = value.Branch;
                    RaisePropertyChanged("Branch");
                }
            }
        }

        public ObservableCollection<ContractMap> Contracts
        {
            get
            {
                ObservableCollection<ContractMap> _contracts = new ObservableCollection<ContractMap>();
                foreach(var item in _agent.Contract)
                {
                    var contract = new ContractMap(item);
                    _contracts.Add(contract);
                }
                return _contracts;
            }
        }

        public ObservableCollection<ContractMap> LoginDatas
        {
            get
            {
                ObservableCollection<ContractMap> _contracts = new ObservableCollection<ContractMap>();
                foreach (var item in _agent.Contract)
                {
                    var contract = new ContractMap(item);
                    _contracts.Add(contract);
                }
                return _contracts;
            }
        }

        #endregion

        #region Commands
        void UpdateArtistNameExecute()
        {
            //Branch branch = new Branch();
            //branch.Address = "kkuku";
            //branch.Name = "fdfdf";
            //branch.Telephone = "3253-343";
            //ins.Branch.Add(branch);
            //ins.SaveChanges();
            
            //ArtistName = string.Format("Elvis ({0})", _count);
        }

        bool CanUpdateArtistNameExecute()
        {
            return true;
        }
        public ICommand UpdateArtistName { get { return new RelayCommand(UpdateArtistNameExecute, CanUpdateArtistNameExecute); } }
        #endregion


    }
}
