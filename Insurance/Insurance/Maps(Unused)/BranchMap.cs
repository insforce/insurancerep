﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class BranchMap: ObservableObject
    {
        #region Members
        private Branch _branch;
        #endregion

        #region Constructors
        public BranchMap()
        {
            _branch = new Branch { Name = "Unknown", Address = "Unknown", Telephone = "Unknown"};
        }

        public BranchMap(Branch branch)
        {
            this._branch = branch;
        }
        #endregion

        #region Properties
        public Branch Branch
        {
            get
            {
                return _branch;
            }
            set
            {
                _branch = value;
            }
        }

        public string Name
        {
            get { return Branch.Name; }
            set
            {
                if (Branch.Name != value)
                {
                    Branch.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        public string Address
        {
            get { return Branch.Address; }
            set
            {
                if (Branch.Address != value)
                {
                    Branch.Address = value;
                    RaisePropertyChanged("Address");
                }
            }
        }

        public string Telephone
        {
            get { return Branch.Telephone; }
            set
            {
                if (Branch.Telephone != value)
                {
                    Branch.Telephone = value;
                    RaisePropertyChanged("Telephone");
                }
            }
        }

        public DirectorMap Director
        {
            get
            {
                return new DirectorMap(_branch.Director);
            }
            set
            {
                if (Director != value)
                {
                    _branch.Director = value.Director;
                    RaisePropertyChanged("Director");
                }
            }
        }

        public ObservableCollection<AgentMap> Agents
        {
            get
            {
                ObservableCollection<AgentMap> _agents = new ObservableCollection<AgentMap>();
                foreach (var item in _branch.Agent)
                {
                    var agent = new AgentMap(item);
                    _agents.Add(agent);
                }
                return _agents;
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
