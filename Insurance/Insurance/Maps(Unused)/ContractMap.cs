﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class ContractMap: ObservableObject
    {
        #region Members
        private Contract _contract;
        #endregion

        #region Constructors
        public ContractMap()
        {
            _contract = new Contract { DateRegistration = new DateTime(), DateStart = new DateTime(), DateFinish = new DateTime(),
                                        ContractNumber = 100000, ContractSeries = "AA", InsuredSum=0 };
        }

        public ContractMap(Contract contract)
        {
            this._contract = contract;
        }
        #endregion

        #region Properties
        public Contract Contract
        {
            get
            {
                return _contract;
            }
            set
            {
                _contract = value;
            }
        }

        public DateTime DateStart
        {
            get { return Contract.DateStart; }
            set
            {
                if (Contract.DateStart != value)
                {
                    Contract.DateStart = value;
                    RaisePropertyChanged("DateStart");
                }
            }
        }

        public DateTime DateRegistration
        {
            get { return Contract.DateRegistration; }
            set
            {
                if (Contract.DateRegistration != value)
                {
                    Contract.DateRegistration = value;
                    RaisePropertyChanged("DateRegistration");
                }
            }
        }

        public DateTime DateFinish
        {
            get { return Contract.DateFinish; }
            set
            {
                if (Contract.DateFinish != value)
                {
                    Contract.DateFinish = value;
                    RaisePropertyChanged("DateFinish");
                }
            }
        }

        public int ContractNumber
        {
            get { return Contract.ContractNumber; }
            set
            {
                if (Contract.ContractNumber != value)
                {
                    Contract.ContractNumber = value;
                    RaisePropertyChanged("ContractNumber");
                }
            }
        }

        public decimal InsuredSum
        {
            get { return Contract.InsuredSum; }
            set
            {
                if (Contract.InsuredSum != value)
                {
                    Contract.InsuredSum = value;
                    RaisePropertyChanged("InsuredSum");
                }
            }
        }

        public string ContractSeries
        {
            get { return Contract.ContractSeries; }
            set
            {
                if (Contract.ContractSeries != value)
                {
                    Contract.ContractSeries = value;
                    RaisePropertyChanged("ContractSeries");
                }
            }
        }

        public AgentMap Agent
        {
            get
            {
                return new AgentMap(_contract.Agent);
            }
            set
            {
                if (Agent != value)
                {
                    _contract.Agent = value.Agent;
                    RaisePropertyChanged("Agent");
                }
            }
        }

        public CustomerMap Customer
        {
            get
            {
                return new CustomerMap(_contract.Customer);
            }
            set
            {
                if (Customer != value)
                {
                    _contract.Customer = value.Customer;
                    RaisePropertyChanged("Customer");
                }
            }
        }

        public InsuranceObjectMap InsuranceObject
        {
            get
            {
                return new InsuranceObjectMap(_contract.InsuranceObject);
            }
            set
            {
                if (InsuranceObject != value)
                {
                    _contract.InsuranceObject = value.InsuranceObject;
                    RaisePropertyChanged("InsuranceObject");
                }
            }
        }

        public ObservableCollection<RiskMap> Risks
        {
            get
            {
                ObservableCollection<RiskMap> _risks = new ObservableCollection<RiskMap>();
                foreach (var item in _contract.Risk)
                {
                    var risk = new RiskMap(item);
                    _risks.Add(risk);
                }
                return _risks;
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
