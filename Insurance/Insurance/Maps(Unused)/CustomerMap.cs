﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class CustomerMap: ObservableObject
    {
        #region Members
        private Customer _customer;
        #endregion

        #region Constructors
        public CustomerMap()
        {
            _customer = new Customer { Name = "Unknown", LegalAddress = "Unknown", ActualAddress = "Unknown", CustomerTypeId = 1 };
        }

        public CustomerMap(Customer customer)
        {
            this._customer = customer;
        }
        #endregion

        #region Properties
        public Customer Customer
        {
            get
            {
                return _customer;
            }
            set
            {
                _customer = value;
            }
        }

        public string Name
        {
            get { return Customer.Name; }
            set
            {
                if (Customer.Name != value)
                {
                    Customer.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        public string ActualAddress
        {
            get { return Customer.ActualAddress; }
            set
            {
                if (Customer.ActualAddress != value)
                {
                    Customer.ActualAddress = value;
                    RaisePropertyChanged("ActualAddress");
                }
            }
        }

        public string LegalAddress
        {
            get { return Customer.LegalAddress; }
            set
            {
                if (Customer.LegalAddress != value)
                {
                    Customer.LegalAddress = value;
                    RaisePropertyChanged("LegalAddress");
                }
            }
        }

        public CustomerTypeMap CustomerType
        {
            get
            {
                return new CustomerTypeMap(_customer.CustomerType);
            }
            set
            {
                if (CustomerType != value)
                {
                    _customer.CustomerType = value.CustomerType;
                    RaisePropertyChanged("CustomerType");
                }
            }
        }

        public PhysicalPersonMap PhysicalPerson
        {
            get
            {
                return new PhysicalPersonMap(_customer.PhysicalPerson);
            }
            set
            {
                if (PhysicalPerson != value)
                {
                    _customer.PhysicalPerson = value.PhysicalPerson;
                    RaisePropertyChanged("PhysicalPerson");
                }
            }
        }

        public LegalPersonMap LegalPerson
        {
            get
            {
                return new LegalPersonMap(_customer.LegalPerson);
            }
            set
            {
                if (LegalPerson != value)
                {
                    _customer.LegalPerson = value.LegalPerson;
                    RaisePropertyChanged("LegalPerson");
                }
            }
        }

        public ObservableCollection<ContractMap> Contracts
        {
            get
            {
                ObservableCollection<ContractMap> _contracts = new ObservableCollection<ContractMap>();
                foreach (var item in _customer.Contract)
                {
                    var contract = new ContractMap(item);
                    _contracts.Add(contract);
                }
                return _contracts;
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
