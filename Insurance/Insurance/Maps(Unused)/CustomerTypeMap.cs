﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class CustomerTypeMap: ObservableObject
    {
        #region Members
        private CustomerType _customerType;
        #endregion

        #region Constructors
        public CustomerTypeMap()
        {
            _customerType = new CustomerType { Name = "Unknown"};
        }

        public CustomerTypeMap(CustomerType customerType)
        {
            this._customerType = customerType;
        }
        #endregion

        #region Properties
        public CustomerType CustomerType
        {
            get
            {
                return _customerType;
            }
            set
            {
                _customerType = value;
            }
        }

        public string Name
        {
            get { return CustomerType.Name; }
            set
            {
                if (CustomerType.Name != value)
                {
                    CustomerType.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        public ObservableCollection<CustomerMap> Customers
        {
            get
            {
                ObservableCollection<CustomerMap> _customers = new ObservableCollection<CustomerMap>();
                foreach (var item in _customerType.Customer)
                {
                    var customer = new CustomerMap(item);
                    _customers.Add(customer);
                }
                return _customers;
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
