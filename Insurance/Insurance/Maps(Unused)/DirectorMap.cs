﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class DirectorMap: ObservableObject
    {
        #region Members
        private Director _director;
        #endregion

        #region Constructors
        public DirectorMap()
        {
            _director = new Director { FIO = "Unknown", Login = "Unknown", Password = "Unknown", BranchId=1 };
        }

        public DirectorMap(Director director)
        {
            this._director = director;
        }
        #endregion

        #region Properties
        public Director Director
        {
            get
            {
                return _director;
            }
            set
            {
                _director = value;
            }
        }

        public string FIO
        {
            get { return Director.FIO; }
            set
            {
                if (Director.FIO != value)
                {
                    Director.FIO = value;
                    RaisePropertyChanged("FIO");
                }
            }
        }

        public string Password
        {
            get { return Director.Password; }
            set
            {
                if (Director.Password != value)
                {
                    Director.Password = value;
                    RaisePropertyChanged("Password");
                }
            }
        }

        public string Login
        {
            get { return Director.Login; }
            set
            {
                if (Director.Login != value)
                {
                    Director.Login = value;
                    RaisePropertyChanged("Login");
                }
            }
        }

        public BranchMap Branch
        {
            get
            {
                return new BranchMap(_director.Branch);
            }
            set
            {
                if (Branch != value)
                {
                    _director.Branch = value.Branch;
                    RaisePropertyChanged("Branch");
                }
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
