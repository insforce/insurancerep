﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class InsuranceObjectMap: ObservableObject
    {
        #region Members
        private InsuranceObject _insuranceObject;
        #endregion

        #region Constructors
        public InsuranceObjectMap()
        {
            _insuranceObject = new InsuranceObject { Description = "Unknown", ContractId = 1, InsuranceObjectTypeId = 1 };
        }

        public InsuranceObjectMap(InsuranceObject insuranceObject)
        {
            this._insuranceObject = insuranceObject;
        }
        #endregion

        #region Properties
        public InsuranceObject InsuranceObject
        {
            get
            {
                return _insuranceObject;
            }
            set
            {
                _insuranceObject = value;
            }
        }

        public string Description
        {
            get { return InsuranceObject.Description; }
            set
            {
                if (InsuranceObject.Description != value)
                {
                    InsuranceObject.Description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }

        public ContractMap Contract
        {
            get
            {
                return new ContractMap(_insuranceObject.Contract);
            }
            set
            {
                if (Contract != value)
                {
                    _insuranceObject.Contract = value.Contract;
                    RaisePropertyChanged("Contract");
                }
            }
        }

        public InsuranceObjectTypeMap InsuranceObjectType
        {
            get
            {
                return new InsuranceObjectTypeMap(_insuranceObject.InsuranceObjectType);
            }
            set
            {
                if (InsuranceObjectType != value)
                {
                    _insuranceObject.InsuranceObjectType = value.InsuranceObjectType;
                    RaisePropertyChanged("InsuranceObjectType");
                }
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
