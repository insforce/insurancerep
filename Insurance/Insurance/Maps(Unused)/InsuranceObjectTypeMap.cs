﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class InsuranceObjectTypeMap: ObservableObject
    {
        #region Members
        private InsuranceObjectType _insuranceObjectType;
        #endregion

        #region Constructors
        public InsuranceObjectTypeMap()
        {
            _insuranceObjectType = new InsuranceObjectType { Name = "Unknown", ObjectTypePrice = 0, TypeInsuranceId = 1 };
        }

        public InsuranceObjectTypeMap(InsuranceObjectType insuranceObjectType)
        {
            this._insuranceObjectType = insuranceObjectType;
        }
        #endregion

        #region Properties
        public InsuranceObjectType InsuranceObjectType
        {
            get
            {
                return _insuranceObjectType;
            }
            set
            {
                _insuranceObjectType = value;
            }
        }

        public string Name
        {
            get { return InsuranceObjectType.Name; }
            set
            {
                if (InsuranceObjectType.Name != value)
                {
                    InsuranceObjectType.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        public decimal ObjectTypePrice
        {
            get { return InsuranceObjectType.ObjectTypePrice; }
            set
            {
                if (InsuranceObjectType.ObjectTypePrice != value)
                {
                    InsuranceObjectType.ObjectTypePrice = value;
                    RaisePropertyChanged("ObjectTypePrice");
                }
            }
        }

        public TypeInsuranceMap TypeInsurance
        {
            get
            {
                return new TypeInsuranceMap(_insuranceObjectType.TypeInsurance);
            }
            set
            {
                if (TypeInsurance != value)
                {
                    _insuranceObjectType.TypeInsurance = value.TypeInsurance;
                    RaisePropertyChanged("TypeInsurance");
                }
            }
        }

        public ObservableCollection<InsuranceObjectMap> InsuranceObjects
        {
            get
            {
                ObservableCollection<InsuranceObjectMap> _insuranceObjects = new ObservableCollection<InsuranceObjectMap>();
                foreach (var item in _insuranceObjectType.InsuranceObject)
                {
                    var insuranceObject = new InsuranceObjectMap(item);
                    _insuranceObjects.Add(insuranceObject);
                }
                return _insuranceObjects;
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
