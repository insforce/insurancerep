﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class LegalPersonMap: ObservableObject
    {
        #region Members
        private LegalPerson _legalPerson;
        #endregion

        #region Constructors
        public LegalPersonMap()
        {
            _legalPerson = new LegalPerson { CertificateBusiness = "Unknown", PaymentDetails = "Unknown", CodeEDRPOU = "Unknown", CustomerId=1 };
        }

        public LegalPersonMap(LegalPerson legalPerson)
        {
            this._legalPerson = legalPerson;
        }
        #endregion

        #region Properties
        public LegalPerson LegalPerson
        {
            get
            {
                return _legalPerson;
            }
            set
            {
                _legalPerson = value;
            }
        }

        public string CertificateBusiness
        {
            get { return LegalPerson.CertificateBusiness; }
            set
            {
                if (LegalPerson.CertificateBusiness != value)
                {
                    LegalPerson.CertificateBusiness = value;
                    RaisePropertyChanged("CertificateBusiness");
                }
            }
        }

        public string PaymentDetails
        {
            get { return LegalPerson.PaymentDetails; }
            set
            {
                if (LegalPerson.PaymentDetails != value)
                {
                    LegalPerson.PaymentDetails = value;
                    RaisePropertyChanged("PaymentDetails");
                }
            }
        }

        public string CodeEDRPOU
        {
            get { return LegalPerson.CodeEDRPOU; }
            set
            {
                if (LegalPerson.CodeEDRPOU != value)
                {
                    LegalPerson.CodeEDRPOU = value;
                    RaisePropertyChanged("CodeEDRPOU");
                }
            }
        }

        public CustomerMap Customer
        {
            get
            {
                return new CustomerMap(_legalPerson.Customer);
            }
            set
            {
                if (Customer != value)
                {
                    _legalPerson.Customer = value.Customer;
                    RaisePropertyChanged("Customer");
                }
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
