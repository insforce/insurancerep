﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class LoginDataMap : ObservableObject
    {
        #region Members
        private LoginData _loginData;
        #endregion

        #region Constructors
        public LoginDataMap()
        {
            _loginData = new LoginData { Login = "Unknown", Password = "Unknown", AgentId=1 };
        }

        public LoginDataMap(LoginData loginData)
        {
            this._loginData = loginData;
        }
        #endregion

        #region Properties
        public LoginData LoginData
        {
            get
            {
                return _loginData;
            }
            set
            {
                _loginData = value;
            }
        }

        public string Login
        {
            get { return LoginData.Login; }
            set
            {
                if (LoginData.Login != value)
                {
                    LoginData.Login = value;
                    RaisePropertyChanged("Login");
                }
            }
        }

        public string Password
        {
            get { return LoginData.Password; }
            set
            {
                if (LoginData.Password != value)
                {
                    LoginData.Password = value;
                    RaisePropertyChanged("Password");
                }
            }
        }

        public AgentMap Agent
        {
            get
            {
                return new AgentMap(_loginData.Agent);
            }
            set
            {
                if (Agent != value)
                {
                    _loginData.Agent = value.Agent;
                    RaisePropertyChanged("Agent");
                }
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
