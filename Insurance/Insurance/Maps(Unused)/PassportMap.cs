﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class PassportMap: ObservableObject
    {
        #region Members
        private Passport _passport;
        #endregion

        #region Constructors
        public PassportMap()
        {
            _passport = new Passport { PassportSeries = "Unknown", PassportNumber = 135348, DataReceipt = new DateTime(), Issued = "Unknown", CustomerId = 1 };
        }

        public PassportMap(Passport passport)
        {
            this._passport = passport;
        }
        #endregion

        #region Properties
        public Passport Passport
        {
            get
            {
                return _passport;
            }
            set
            {
                _passport = value;
            }
        }

        public string PassportSeries
        {
            get { return Passport.PassportSeries; }
            set
            {
                if (Passport.PassportSeries != value)
                {
                    Passport.PassportSeries = value;
                    RaisePropertyChanged("PassportSeries");
                }
            }
        }

        public int PassportNumber
        {
            get { return Passport.PassportNumber; }
            set
            {
                if (Passport.PassportNumber != value)
                {
                    Passport.PassportNumber = value;
                    RaisePropertyChanged("PassportNumber");
                }
            }
        }

        public DateTime DataReceipt
        {
            get { return Passport.DataReceipt; }
            set
            {
                if (Passport.DataReceipt != value)
                {
                    Passport.DataReceipt = value;
                    RaisePropertyChanged("DataReceipt");
                }
            }
        }

        public string Issued
        {
            get { return Passport.Issued; }
            set
            {
                if (Passport.Issued != value)
                {
                    Passport.Issued = value;
                    RaisePropertyChanged("Issued");
                }
            }
        }

        public PhysicalPersonMap PhysicalPerson
        {
            get
            {
                return new PhysicalPersonMap(_passport.PhysicalPerson);
            }
            set
            {
                if (PhysicalPerson != value)
                {
                    _passport.PhysicalPerson = value.PhysicalPerson;
                    RaisePropertyChanged("PhysicalPerson");
                }
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
