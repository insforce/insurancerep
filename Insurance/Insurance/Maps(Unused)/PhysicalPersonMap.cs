﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class PhysicalPersonMap: ObservableObject
    {
        #region Members
        private PhysicalPerson _physicalPerson;
        #endregion

        #region Constructors
        public PhysicalPersonMap()
        {
            _physicalPerson = new PhysicalPerson { IdentificationCode = 1000000000, DriverSeries = "Unknown", CustomerId = 1 };
        }

        public PhysicalPersonMap(PhysicalPerson physicalPerson)
        {
            this._physicalPerson = physicalPerson;
        }
        #endregion

        #region Properties
        public PhysicalPerson PhysicalPerson
        {
            get
            {
                return _physicalPerson;
            }
            set
            {
                _physicalPerson = value;
            }
        }

        public long IdentificationCode
        {
            get { return PhysicalPerson.IdentificationCode; }
            set
            {
                if (PhysicalPerson.IdentificationCode != value)
                {
                    PhysicalPerson.IdentificationCode = value;
                    RaisePropertyChanged("IdentificationCode");
                }
            }
        }

        public string DriverSeries
        {
            get { return PhysicalPerson.DriverSeries; }
            set
            {
                if (PhysicalPerson.DriverSeries != value)
                {
                    PhysicalPerson.DriverSeries = value;
                    RaisePropertyChanged("DriverSeries");
                }
            }
        }

        public int? DriverNumber
        {
            get { return PhysicalPerson.DriverNumber; }
            set
            {
                if (PhysicalPerson.DriverNumber != value)
                {
                    PhysicalPerson.DriverNumber = value;
                    RaisePropertyChanged("DriverNumber");
                }
            }
        }

        public CustomerMap Customer
        {
            get
            {
                return new CustomerMap(_physicalPerson.Customer);
            }
            set
            {
                if (Customer != value)
                {
                    _physicalPerson.Customer = value.Customer;
                    RaisePropertyChanged("Customer");
                }
            }
        }

        public PassportMap Passport
        {
            get
            {
                return new PassportMap(_physicalPerson.Passport);
            }
            set
            {
                if (Passport != value)
                {
                    _physicalPerson.Passport = value.Passport;
                    RaisePropertyChanged("Passport");
                }
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
