﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class RiskMap: ObservableObject
    {
        #region Members
        private Risk _risk;
        #endregion

        #region Constructors
        public RiskMap()
        {
            _risk = new Risk { Name = "Unknown", RiscPrice = 1, TypeInsuranceId = 1 };
        }

        public RiskMap(Risk risk)
        {
            this._risk = risk;
        }
        #endregion

        #region Properties
        public Risk Risk
        {
            get
            {
                return _risk;
            }
            set
            {
                _risk = value;
            }
        }

        public string Name
        {
            get { return Risk.Name; }
            set
            {
                if (Risk.Name != value)
                {
                    Risk.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        public decimal RiscPrice
        {
            get { return Risk.RiscPrice; }
            set
            {
                if (Risk.RiscPrice != value)
                {
                    Risk.RiscPrice = value;
                    RaisePropertyChanged("RiscPrice");
                }
            }
        }

        public TypeInsuranceMap TypeInsurance
        {
            get
            {
                return new TypeInsuranceMap(_risk.TypeInsurance);
            }
            set
            {
                if (TypeInsurance != value)
                {
                    _risk.TypeInsurance = value.TypeInsurance;
                    RaisePropertyChanged("TypeInsurance");
                }
            }
        }

        public ObservableCollection<ContractMap> Contracts
        {
            get
            {
                ObservableCollection<ContractMap> _contracts = new ObservableCollection<ContractMap>();
                foreach (var item in _risk.Contract)
                {
                    var contract = new ContractMap(item);
                    _contracts.Add(contract);
                }
                return _contracts;
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
