﻿using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace Insurance.DataAccess
{
    public class TypeInsuranceMap : ObservableObject
    {
        #region Members
        private TypeInsurance _typeInsurance;
        #endregion

        #region Constructors
        public TypeInsuranceMap()
        {
            _typeInsurance = new TypeInsurance { Name = "Unknown", TariffRate = 1};
        }

        public TypeInsuranceMap(TypeInsurance typeInsurance)
        {
            this._typeInsurance = typeInsurance;
        }
        #endregion

        #region Properties
        public TypeInsurance TypeInsurance
        {
            get
            {
                return _typeInsurance;
            }
            set
            {
                _typeInsurance = value;
            }
        }

        public string Name
        {
            get { return TypeInsurance.Name; }
            set
            {
                if (TypeInsurance.Name != value)
                {
                    TypeInsurance.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        public decimal TariffRate
        {
            get { return TypeInsurance.TariffRate; }
            set
            {
                if (TypeInsurance.TariffRate != value)
                {
                    TypeInsurance.TariffRate = value;
                    RaisePropertyChanged("TariffRate");
                }
            }
        }

        public ObservableCollection<InsuranceObjectTypeMap> InsuranceObjectTypes
        {
            get
            {
                ObservableCollection<InsuranceObjectTypeMap> _insuranceObjectTypes = new ObservableCollection<InsuranceObjectTypeMap>();
                foreach (var item in _typeInsurance.InsuranceObjectType)
                {
                    var insuranceObjectType = new InsuranceObjectTypeMap(item);
                    _insuranceObjectTypes.Add(insuranceObjectType);
                }
                return _insuranceObjectTypes;
            }
        }

        public ObservableCollection<RiskMap> InsuranceObjects
        {
            get
            {
                ObservableCollection<RiskMap> _risks = new ObservableCollection<RiskMap>();
                foreach (var item in _typeInsurance.Risk)
                {
                    var risk = new RiskMap(item);
                    _risks.Add(risk);
                }
                return _risks;
            }
        }
        #endregion

        #region Commands

        #endregion
    }
}
