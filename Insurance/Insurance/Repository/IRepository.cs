﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Insurance.DataAccess;
using System.Linq.Expressions;

namespace Insurance.Repositories
{
    public interface IRepository
    {
        Agent AuthorizedAgent { get; set; }
        Director AuthorizedDirector { get; set; }

        IQueryable<Agent> Agents { get; }
        IQueryable<Branch> Branches { get; }
        IQueryable<Contract> Contercts { get; }
        IQueryable<Customer> Customers { get; }
        IQueryable<CustomerType> CustomerTypes { get; }
        IQueryable<Director> Directors { get; }
        IQueryable<InsuranceObject> InsuranceObjects  { get; }
        IQueryable<InsuranceObjectType> InsuranceObjectsTypes { get; }
        IQueryable<TypeInsurance> InsuranseTypes { get; }
        IQueryable<LegalPerson> LegalPersons { get; }
        IQueryable<LoginData> LoginData { get; }
        IQueryable<Passport> PasportData { get; }
        IQueryable<PhysicalPerson> PhysicalPersons { get; }
        IQueryable<Risk> Risks { get; }

        Agent GetAgentById(int agentId); 
        IQueryable<Agent> GetAgentsOfBranch(int branchId);
        LoginData GetAgentLoginData(string login);         
        Branch GetBranchById(int id);
        IQueryable<Contract> GetContractsOfAgent(int id);     
        Director GetDirectorByLogin(string login);
        Passport GetPasportDataById(int id);
        PhysicalPerson GetPhysicalPersonById(int id);

        void AddAgent(Agent agent);
        void DeleteAgent(Agent agent);
        void ChangeAgent(Agent agent);
    }

    //public interface IRepository<T> : IRepository where T : class
    //{
    //    void Add(T entity);
    //    void Delete(T entity);
    //    void Change(T entity_for_delete, T entity_for_add);
    //    T Get(Expression<Func<T, Boolean>> where);
    //    IEnumerable<T> GetAll();
    //    IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
    //}
}
