﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Insurance.DataAccess;

namespace Insurance.Repositories
{
    public class Repository : IRepository
    {
        private readonly DataContext _dataContext;
        private Agent _authorizedAgent;
        private Director _authorizedDirector;

        public Repository()
        {
            _dataContext = new DataContext();
            LoadContext();
        }

        public Agent AuthorizedAgent
        {
            get { return _authorizedAgent; }
            set { _authorizedAgent = value; }
        }

        public Director AuthorizedDirector
        {
            get { return _authorizedDirector; }
            set { _authorizedDirector = value; }
        }

        public IQueryable<Agent> Agents
        {
            get { return _dataContext.Agent; }
        }

        public IQueryable<Branch> Branches
        {
            get { return _dataContext.Branch; }
        }

        public IQueryable<Contract> Contercts
        {
            get { return _dataContext.Contract; }
        }

        public IQueryable<Customer> Customers
        {
            get { return _dataContext.Customer; }
        }

        public IQueryable<CustomerType> CustomerTypes
        {
            get { return _dataContext.CustomerType; }
        }

        public IQueryable<Director> Directors
        {
            get { return _dataContext.Director; }
        }

        public IQueryable<InsuranceObject> InsuranceObjects
        {
            get { return _dataContext.InsuranceObject; }
        }

        public IQueryable<InsuranceObjectType> InsuranceObjectsTypes
        {
            get { return _dataContext.InsuranceObjectType; }
        }

        public IQueryable<TypeInsurance> InsuranseTypes
        {
            get { return _dataContext.TypeInsurance; }
        }

        public IQueryable<LegalPerson> LegalPersons
        {
            get { return _dataContext.LegalPerson; }
        }

        public IQueryable<LoginData> LoginData
        {
            get { return _dataContext.LoginData; }
        }

        public IQueryable<Passport> PasportData
        {
            get { return _dataContext.Passport; }
        }

        public IQueryable<PhysicalPerson> PhysicalPersons
        {
            get { return _dataContext.PhysicalPerson; }
        }

        public IQueryable<Risk> Risks
        {
            get { return _dataContext.Risk; }
        }

        public Agent GetAgentById(int agentId)
        {
            var item = _dataContext.Agent.FirstOrDefault(a => a.AgentId == agentId);
            return item;
        }

        public IQueryable<Agent> GetAgentsOfBranch(int branchId)
        {
            var collection = _dataContext.Agent.Where(a => a.BranchId == branchId);
            return collection;
        }

        public LoginData GetAgentLoginData(string login)
        {
            var item = _dataContext.LoginData.FirstOrDefault(a => a.Login.Equals(login));
            return item;
        }

        public Branch GetBranchById(int id)
        {
            var item = _dataContext.Branch.FirstOrDefault(a => a.BranchId == id);
            return item;

        }

        public IQueryable<Contract> GetContractsOfAgent(int id)
        {
            var collection = _dataContext.Contract.Where(a => a.AgentId == id);
            return collection;
        }

        public Director GetDirectorByLogin(string login)
        {
            var item = _dataContext.Director.FirstOrDefault(d => d.Login == login);
            return item;
        }

        public Passport GetPasportDataById(int id)
        {
            var item = _dataContext.Passport.FirstOrDefault(p => p.CustomerId == id);
            return item;
        }

        public PhysicalPerson GetPhysicalPersonById(int id)
        {
            var item = _dataContext.PhysicalPerson.FirstOrDefault(p => p.CustomerId == id);
            return item;
        }

        public void AddAgent(Agent agent)
        {
            _dataContext.Agent.Add(agent);
            _dataContext.SaveChanges();
        }

        public void DeleteAgent(Agent agent)
        {
            _dataContext.Agent.Remove(agent);
            _dataContext.SaveChanges();
        }

        public void ChangeAgent(Agent agent)
        {
            _dataContext.Entry(agent).State = EntityState.Modified;
            _dataContext.SaveChanges();
        }

        /// <summary>
        /// Опасное место!
        /// </summary>
        private void LoadContext()
        {
            _dataContext.Agent.Load();
            _dataContext.Branch.Load();
            _dataContext.Contract.Load();
            _dataContext.Customer.Load();
            _dataContext.CustomerType.Load();
            _dataContext.Director.Load();
            _dataContext.InsuranceObject.Load();
            _dataContext.InsuranceObjectType.Load();
            _dataContext.LegalPerson.Load();
            _dataContext.LoginData.Load();
            _dataContext.Passport.Load();
            _dataContext.PhysicalPerson.Load();
            _dataContext.Risk.Load();
            _dataContext.TypeInsurance.Load();
        }
    }

    //public class Repository<T> : Repository, IRepository<T> where T:class
    //{
    //    protected DbSet<T> table;

    //    public Repository()
    //    {
    //        _dataContext = new DataContext();
    //        table = _dataContext.Set<T>();
    //    }
    //    public void Add(T entity)
    //    {
    //        if(entity!=null)
    //        {
    //            table.Add(entity);
    //        }
    //    }

    //    public void Delete(T entity)
    //    {
    //        if (entity != null)
    //        {
    //            table.Remove(entity);
    //        }
    //    }

    //    public void Change(T entity_for_delete, T entity_for_add)
    //    {
    //        if (entity_for_delete != null && entity_for_add!=null)
    //        {
    //            Delete(entity_for_delete);
    //            Add(entity_for_add);
    //        }
    //    }

    //    public T Get(System.Linq.Expressions.Expression<Func<T, bool>> where)
    //    {
    //        return table.Where(where).FirstOrDefault();
    //    }

    //    public IEnumerable<T> GetAll()
    //    {
    //        return table;
    //    }

    //    public IEnumerable<T> GetMany(System.Linq.Expressions.Expression<Func<T, bool>> where)
    //    {
    //        return table.Where(where);
    //    }
    //}
}
