﻿using Insurance.AuthenticationService;
using MicroMvvm;
using System.Windows.Input;
using System;
using Insurance.Views;

namespace Insurance.ViewModel
{
    public class AgentLoginViewModel : BaseLoginViewModel
    {

        public AgentLoginViewModel(IAuthenticationService service)
        {
            _authenticationService = service;
        }

        protected override void Authorize()
        {
            {
                if (!ValidateLoginData())
                {
                    System.Windows.MessageBox.Show("Пропущено одно из полей.");
                    return;
                }
                else
                if (!_authenticationService.AuthorizeAgent(_login, _password))
                {
                    System.Windows.MessageBox.Show("Неверно введеные данные.");
                    return;
                }


                AgentView window = new AgentView();
                window.Show();
                System.Windows.Application.Current.MainWindow.Close();
                System.Windows.Application.Current.MainWindow = window;

                
            }
        }
    }
}