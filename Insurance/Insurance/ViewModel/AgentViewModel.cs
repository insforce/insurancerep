﻿using Insurance.Repositories;
using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Insurance.DataAccess;

namespace Insurance.ViewModel
{
    public class AgentViewModel : BaseViewModel
    {
        private TypeInsurance _selectedInsType;
        private CustomerType _selectedCustType;
        private ObservableCollection<TypeInsurance> _instypes;
        private ObservableCollection<CustomerType> _custtypes;

        public AgentViewModel(IRepository repository)
        {
            _repository = repository;
            Init();
        }

        public CustomerType SelectedCustomerType
        {
            get
            {
                return _selectedCustType;
            }

            set
            {
                _selectedCustType = value;
                RaisePropertyChanged("SelectedCustomerType");
            }
        }
        public TypeInsurance SelectedInsuranceType
        {
            get
            {
                return _selectedInsType;
            }

            set
            {
                _selectedInsType = value;
                RaisePropertyChanged("SelectedInsuranceType");
            }
        }

        public ObservableCollection<TypeInsurance> InsuranseTypes
        {
            get{ return _instypes; }
        }      
        public ObservableCollection<CustomerType> CustomerTypes
        {
            get { return _custtypes; }
        }       

        protected override void Init()
        {
            _instypes = new ObservableCollection
                <TypeInsurance>(_repository.InsuranseTypes);
            _custtypes = new ObservableCollection
                <CustomerType>(_repository.CustomerTypes);
        }

    }
}
