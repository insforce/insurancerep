﻿using Insurance.AuthenticationService;
using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Insurance.ViewModel
{
    public abstract class BaseLoginViewModel : ObservableObject
    {

       

        protected string _login = string.Empty;
        protected string _password = string.Empty;
        protected IAuthenticationService _authenticationService;
        protected ICommand _loginCommand;

        public string Login
        {
            get { return _login;  }
            set { _login = value; RaisePropertyChanged("Login"); }
        } 

        public string Password
        {
            get { return _password; }
            set { _password = value; RaisePropertyChanged("Password"); }
        }

        protected abstract void Authorize();

        protected bool ValidateLoginData()
        {

            if (_login.Equals(string.Empty))
                return false;
            if (_password.Equals(string.Empty))
                return false;
            return true;

        }

        public ICommand LoginCommand
        {
            get
            {
                _loginCommand = _loginCommand ??
                    new RelayCommand(Authorize);
                return _loginCommand;
            }
        }
    }
}
