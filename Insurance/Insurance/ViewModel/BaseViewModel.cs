﻿using Insurance.Repositories;
using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Insurance.ViewModel
{
    public abstract class BaseViewModel : ObservableObject
    {

        protected IRepository _repository;
        protected abstract void Init();



    }
}
