﻿using Insurance.AuthenticationService;
using Insurance.Views;

namespace Insurance.ViewModel
{
    public class DirectorLoginViewModel : BaseLoginViewModel
    {

        public DirectorLoginViewModel(IAuthenticationService service)
        {
            _authenticationService = service;
        }

        protected override void Authorize()
        {
            {
                if (!ValidateLoginData())
                {
                    System.Windows.MessageBox.Show("Пропущено одно из полей.");
                    return;
                }
                else if (!_authenticationService.AuthorizeDirector(_login, _password))
                {
                        System.Windows.MessageBox.Show("Неверно введеные данные.");
                        return;
                    
                }

                DirectorView window = new DirectorView();
                window.Show();
                System.Windows.Application.Current.MainWindow.Close();
                System.Windows.Application.Current.MainWindow = window;

            }
        }

    }
}