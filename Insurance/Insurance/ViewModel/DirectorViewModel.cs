﻿using Insurance.DataAccess;
using Insurance.Repositories;
using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Insurance.ViewModel
{
    public class DirectorViewModel : BaseViewModel
    {
        private string _surnameAdd;
        private string _nameAdd;
        private string _fatherAdd;
        private string _addressAdd;
        private string _telephoneAdd;

        private string _surnameEdit;
        private string _nameEdit;
        private string _fatherEdit;

        private Agent _selectedAgentForDelete;
        private Agent _selectedAgentForUpdate;
        private ObservableCollection<Agent> _agents;

        public DirectorViewModel(IRepository repository)
        {
            _repository = repository;
            Init();
        }

        #region Properties
        public string SurnameAdd
        {
            get
            {
                return _surnameAdd;
            }
            set
            {
                _surnameAdd = value;
            }
        }

        public string NameAdd
        {
            get
            {
                return _nameAdd;
            }
            set
            {
                _nameAdd = value;
            }
        }

        public string FatherAdd
        {
            get
            {
                return _fatherAdd;
            }
            set
            {
                _fatherAdd = value;
            }
        }

        public string AddressAdd
        {
            get
            {
                return _addressAdd;
            }
            set
            {
                _addressAdd = value;
            }
        }

        public string TelephoneAdd
        {
            get
            {
                return _telephoneAdd;
            }
            set
            {
                _telephoneAdd = value;
            }
        }

        public string SurnameEdit
        {
            get
            {
                return _surnameEdit;
            }
            set
            {
                _surnameEdit = value;
                RaisePropertyChanged("SurnameEdit");
            }
        }

        public string NameEdit
        {
            get
            {
                return _nameEdit;
            }
            set
            {
                _nameEdit = value;
                RaisePropertyChanged("NameEdit");
            }
        }

        public string FatherEdit
        {
            get
            {
                return _fatherEdit;
            }
            set
            {
                _fatherEdit = value;
                RaisePropertyChanged("FatherEdit");
            }
        }

        public string AddressEdit
        {
            get
            {
                if (SelectedAgentForUpdate != null)
                {
                    return SelectedAgentForUpdate.Address;
                }
                return String.Empty;
            }
            set
            {
                SelectedAgentForUpdate.Address = value;
                RaisePropertyChanged("AddressEdit");
                RaisePropertyChanged("SelectedAgentForUpdate");
            }
        }

        public string TelephoneEdit
        {
            get
            {
                if (SelectedAgentForUpdate != null)
                {
                    return SelectedAgentForUpdate.Telephone;
                }
                return String.Empty;
            }
            set
            {
                SelectedAgentForUpdate.Telephone = value;
                RaisePropertyChanged("TelephoneEdit");
                RaisePropertyChanged("SelectedAgentForUpdate");
            }
        }

        public Agent SelectedAgentForDelete
        {
            get
            {
                return _selectedAgentForDelete;
            }

            set
            {
                _selectedAgentForDelete = value;
            }
        }

        public Agent SelectedAgentForUpdate
        {
            get
            {
                return _selectedAgentForUpdate;
            }

            set
            {
                _selectedAgentForUpdate = value;
                String[] fio = value.FIO.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                SurnameEdit = fio[0];
                NameEdit = fio[1];
                FatherEdit = fio[2];
                AddressEdit = value.Address;
                TelephoneEdit = value.Telephone;
            }
        }


        public ObservableCollection<Agent> Agents
        {
            get { return _agents; }
        } 
        #endregion

        protected override void Init()
        {
            _agents = new ObservableCollection<Agent>(_repository.Agents);
        }

        #region Command
        void AddAgentExecute()
        {
            Agent agent = new Agent();
            agent.FIO = SurnameAdd + " " + NameAdd + " " + FatherAdd;
            agent.Telephone = TelephoneAdd;
            agent.Address = AddressAdd;
            agent.Branch = new Branch(1, "unk", "unk", "unk");//_repository.AuthorizedDirector.Branch; //
            _repository.AddAgent(agent);
            Agents.Add(agent);
            RaisePropertyChanged("Agents");
        }

        bool CanAddAgentExecute()
        {
            if (String.Compare(SurnameAdd, "") == 0 && String.Compare(NameAdd, "") == 0 && String.Compare(FatherAdd, "") == 0 && String.Compare(AddressAdd, "") == 0)
                return false;
            return true;
        }

        public ICommand AddAgent { get { return new RelayCommand(AddAgentExecute, CanAddAgentExecute); } }

        void DeleteAgentExecute()
        {
            _repository.DeleteAgent(SelectedAgentForDelete);
            Agents.Remove(SelectedAgentForDelete);
            RaisePropertyChanged("Agents");
        }

        bool CanDeleteAgentExecute()
        {
            if(SelectedAgentForDelete==null)
            {
                return false;
            }
            return true;
        }

        public ICommand DeleteAgent { get { return new RelayCommand(DeleteAgentExecute, CanDeleteAgentExecute); } }

        void ChangeAgentExecute()
        {
            _repository.ChangeAgent(SelectedAgentForUpdate);
            Agent agent = Agents.FirstOrDefault(x=> x.AgentId==SelectedAgentForUpdate.AgentId);
            agent.FIO = SurnameAdd + " " + NameAdd + " " + FatherAdd;
            agent.Telephone = TelephoneAdd;
            agent.Address = AddressAdd;
            RaisePropertyChanged("Agents");
        }

        bool CanChangeAgentExecute()
        {
            if (SelectedAgentForUpdate == null)
            {
                return false;
            }
            return true;
        }

        public ICommand ChangeAgent { get { return new RelayCommand(ChangeAgentExecute, CanChangeAgentExecute); } }
        #endregion
    }
}
