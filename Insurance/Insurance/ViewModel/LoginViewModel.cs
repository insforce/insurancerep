﻿using Insurance.Views.LoginWindows;
using MicroMvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Insurance.ViewModel
{
    public class LoginViewModel : ObservableObject
    {

        private ICommand _navigateAgentCommand;
        private ICommand _navigateDirectolCommand;
        protected ContentControl currentView = new AgentLogin();

        public ContentControl CurrentView
        {
            get
            {
                return currentView;
            }

            set
            {
                currentView = value;
                RaisePropertyChanged("CurrentView");
            }
        }

        public ICommand NavigateAgentCommand
        {
            get
            {
                _navigateAgentCommand = _navigateAgentCommand ??
                    new RelayCommand(() => CurrentView = new AgentLogin());
                return _navigateAgentCommand;
            }
        }
        public ICommand NavigateDirectorCommand
        {
            get
            {
                _navigateDirectolCommand = _navigateDirectolCommand ??
                    new RelayCommand(() => CurrentView = new DirectorLogin());
                return _navigateDirectolCommand;
            }
        }





    }
}
