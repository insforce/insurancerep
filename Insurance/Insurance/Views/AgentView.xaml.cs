﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Office.Interop.Word;

namespace Insurance.Views
{
    //public enum CustomerType
    //{
    //    [Description("Фізична")]
    //    Physical,
    //    [Description("Юридична")]
    //    Legal
    //}

    //public static class ReflectionHelpers
    //{
    //    public static string GetCustomDescription(object objEnum)
    //    {
    //        var fi = objEnum.GetType().GetField(objEnum.ToString());
    //        var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
    //        return (attributes.Length > 0) ? attributes[0].Description : objEnum.ToString();
    //    }

    //    public static string Description(this Enum value)
    //    {
    //        return GetCustomDescription(value);
    //    }
    //}


    /// <summary>
    /// Interaction logic for AgentView.xaml
    /// </summary>
    public partial class AgentView : System.Windows.Window
    {
        public AgentView()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
          
        }

        private void LabelPassport_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void LabelObject_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void LabelRisk_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnPassport_Click(object sender, RoutedEventArgs e)
        {
            var window = new PassportView();
            window.ShowDialog();
        }

        private void btnRisks_Click(object sender, RoutedEventArgs e)
        {
            var window = new RiskView();
            window.ShowDialog();
        }

        private void btnInsuranceObject_Click(object sender, RoutedEventArgs e)
        {

        }

        private void lstRisks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Director_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnSaveAdd_Click(object sender, RoutedEventArgs e)
        {

            object fileName = @"C:\Users\maksibonus\Desktop\Проект\Insurance\Insurance\Insurance\doc1.docx";

            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application
            {
                Visible = true
            };
            try
            {
                Document aDoc = wordApp.Documents.Open(ref fileName, ReadOnly: false, Visible: true);
                aDoc.Activate();

                Find fnd = wordApp.ActiveWindow.Selection.Find;

                fnd.ClearFormatting();
                fnd.Replacement.ClearFormatting();
                fnd.Forward = true;
                fnd.Wrap = WdFindWrap.wdFindContinue;

                fnd.Text = "[FIO]";
                if (txtFIO.Text.Length != 0)
                {
                    fnd.Replacement.Text = txtFIO.Text;
                }
                else
                {
                    fnd.Replacement.Text = txtNameCompany.Text;
                }

                fnd.Text = "[STARTDATE]";
                string date = dataStart.Text;
                fnd.Replacement.Text = date;

                fnd.Text = "[FIO]";
                if (txtFIO.Text.Length != 0)
                {
                    fnd.Replacement.Text = txtFIO.Text;
                }
                else
                {
                    fnd.Replacement.Text = txtNameCompany.Text;
                }


                //fnd.Text = "[N]";
                //fnd.Replacement.Text = 

                fnd.Execute(Replace: WdReplace.wdReplaceAll);

            }
            catch (Exception error)
            {
                error.Message.ToString();
                MessageBox.Show("Ви не зможете редагувати новий договір");
            }
        }
    }
}
