﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Insurance.DataAccess;
using Insurance.Repositories;
using System.Data;
using Microsoft.Office.Interop.Word;
using System.Reflection;

namespace Insurance.Views
{
    /// <summary>
    /// Interaction logic for DirectorView.xaml
    /// </summary>
    public partial class DirectorView : System.Windows.Window
    {
        public DirectorView()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Branch branch = new Branch();
            //branch.Address = "kkuku";
            //branch.Name = "fdfdf";
            //branch.Telephone = "3253-343";
            //ins.Branch.Add(branch);
            //ins.SaveChanges();
        }

        private void btnCalculate_Click(object sender, RoutedEventArgs e)
        {

        }


        /// <summary>
        /// Report about salary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrint_Click(object sender, EventArgs e)
        {
            //CreateReport();
            //ExportInWord((DataTable)dataGridViewReport.DataSource);
        }

        //public void ExportInWord(DataTable t)
        //{
        //    object ms = Missing.Value;
        //    Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
        //    app.Visible = true;

        //    t.Columns.Remove("Дата_продажи");
        //    Microsoft.Office.Interop.Word.Document doc = app.Documents.Add(ref ms, ref ms, ref ms, ref ms);
        //    doc.Activate();
        //    // вывод имени таблицы в формате заголовка         
        //    doc.ActiveWindow.Selection.Font.Size = 20;
        //    doc.ActiveWindow.Selection.Font.Bold = 30;
        //    doc.ActiveWindow.Selection.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //    doc.ActiveWindow.Selection.TypeText(t.TableName);
        //    doc.Application.Selection.TypeParagraph();
        //    doc.ActiveWindow.Selection.TypeText("о продажах за период");
        //    doc.Application.Selection.TypeParagraph();
        //    doc.ActiveWindow.Selection.TypeText("c " + dateTimeFrom.Value.ToShortDateString() + " до "
        //        + dateTimeTo.Value.ToShortDateString());

        //    doc.Application.Selection.TypeParagraph();
        //    doc.ActiveWindow.Selection.TypeText("по категории: " + comboBoxCategory.Text);

        //    doc.Application.Selection.TypeParagraph();
        //    doc.ActiveWindow.Selection.Font.Bold = 0;

        //    // изменение форматирования для текста документа
        //    doc.ActiveWindow.Selection.Font.Size = 14;
        //    doc.Application.Selection.TypeParagraph();

        //    // количество строк в таблице
        //    int kRows = t.Rows.Count;

        //    // количество столбцов в таблице
        //    int kColumns = t.Columns.Count;

        //    // добавление таблицы в документ
        //    Microsoft.Office.Interop.Word.Table table = doc.Tables.Add(doc.Application.Selection.Range, kRows + 1, kColumns, ref ms, ref ms);

        //    // стиль границ таблицы
        //    table.Borders.OutsideLineStyle = WdLineStyle.wdLineStyleThinThickMedGap;
        //    table.Borders.OutsideColor = WdColor.wdColorBlueGray;
        //    table.Borders.InsideLineStyle = WdLineStyle.wdLineStyleInset;
        //    table.Borders.InsideColor = WdColor.wdColorBlueGray;

        //    // вывод заголовков столбцов таблицы
        //    for (int j = 0; j < kColumns; j++)
        //    {
        //        table.Cell(1, j + 1).Range.Text = t.Columns[j].Caption;
        //        table.Cell(1, j + 1).Range.Bold = 30;
        //    }

        //    DateTime d = new DateTime();
        //    // заполнение таблицы значениями
        //    for (int i = 1; i <= kRows; i++)
        //    {
        //        for (int j = 0; j < kColumns; j++)
        //        {
        //            if (t.Columns[j].DataType.ToString() == "System.DateTime")
        //            {
        //                d = Convert.ToDateTime(t.Rows[i - 1][j]);
        //                table.Cell(i + 1, j + 1).Range.Text = d.Date.ToString("d/MM/yy");
        //            }
        //            else
        //                table.Cell(i + 1, j + 1).Range.Text = t.Rows[i - 1][j].ToString();
        //            table.Cell(i + 1, j + 1).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
        //        }
        //    }

        //    // выход за пределы таблицы
        //    object o1 = WdUnits.wdLine;
        //    object o2 = doc.Content.End;
        //    doc.Application.Selection.MoveDown(ref o1, ref o2, ref ms);

        //    doc.ActiveWindow.Selection.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
        //    doc.Application.Selection.TypeParagraph();
        //    doc.Application.Selection.TypeText("Итого: " + sum.ToString("N2") + " грн");

        //    doc.Application.Selection.TypeParagraph();
        //    doc.Application.Selection.TypeParagraph();

        //    // вывод текущей даты и места для подписи
        //    doc.ActiveWindow.Selection.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
        //    doc.ActiveWindow.Selection.Font.Italic = 10;
        //    doc.ActiveWindow.Selection.TypeText("Дата: ");
        //    doc.Application.Selection.TypeParagraph();
        //    doc.ActiveWindow.Selection.TypeText("\t" + DateTime.Today.ToString("dd MMMM yyyy") + " года");
        //    doc.Application.Selection.TypeParagraph();
        //    doc.Application.Selection.TypeParagraph();
        //    doc.ActiveWindow.Selection.TypeText("Директор:");
        //    doc.Application.Selection.TypeParagraph();
        //    doc.ActiveWindow.Selection.TypeText("\t_________________");
        //    doc.Application.Selection.TypeParagraph();

        //    app.Activate();
        //}

    }
}
