﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using test_data_generator.DataAccess;

namespace test_data_generator.DataDictionary
{
    public class RandomDataProvider
    {

        private Random _randomizer;

        private string[] Names = new string[]
        {
            "Сергій", "Олександр", "Максим", "Валентин",
            "Андрій", "Олексій", "Гліб", "Артем", "Володимир"
        };

        private string[] Paronimics = new string[]
        {
            "Сергійович", "Олександрович", "Максимович", "Валентинович",
            "Андрійович", "Олексійович"
        };

        private string[] SirNames = new string[]
        {
            "Шевченко", "Возняк", "Капуста", "Кактус",
            "Федік", "Джобс", "Малевіч", "Моцарт", "Бах",
            "Колеснік"
        };

        private string[] FirmsTitle = new string[]
        {
            "Акварель","Симбиан","Майкрософт","НекроФирм","ВоенТорг"
        };

        private string[] LifeInsObjectTyps = new string[]
        {
            "Особисте страхування", "Колективне страхування"
        };

        private string[] AutoInsObjectTyps = new string[]
        {
            "Легкове авто","Грузовое авто"
        };

        private string[] PrivareObjType = new string[]
        {
            "Будинок", "Квартира"
        };

        private string[] DescSample = new string[]
        {
             "Дорогий(а)","Дешевий(а)"
        };

        private string[] AddressSamples = new string[]
        {
            "Сумська", "Свердлова", "Клочківська", "Московська", "Кадастрова",
            "Проща Руднєва", "Патріотична", "Гладка", "Холодни", "Гарна"
        };

        private string[] AutoRisc = new string[]
        {
            "Викрадення","ДТП"
        };

        private string[] LifeRisc = new string[]
        {
            "Втрата працездатності","Смерть"
        };

        private string[] ObjectRisk = new string[]
        {
            "Втрата", "Руйнування в наслыдок нещасного випадку"
        };


        public RandomDataProvider()
        {
            _randomizer = new Random();
        }
        
        internal string GetRandomAuto()
        {
            int i = _randomizer.Next(0, AutoInsObjectTyps.Length - 1);
            return AutoInsObjectTyps[i];
        }
        internal string GetRandomDesc()
        {
            int i = _randomizer.Next(0, DescSample.Length);
            return DescSample[i];
        }       
        internal string GetRandomFio()
        {
            return string.Format("{0} {1} {2}",
                SirNames[_randomizer.Next(0, SirNames.Length - 1)],
                Names[_randomizer.Next(0, SirNames.Length - 1)],
                Paronimics[_randomizer.Next(0, Paronimics.Length - 1)]);
        }
        internal string GetRandomCertificateBusiness()
        {
            return string.Format("{0}{1} {2}",
                (char)_randomizer.Next(0x0410, 0x44F),
                _randomizer.Next(10, 99),
                _randomizer.Next(100000, 999999));
        }
        internal string GetRandomAddress()
        {
            return string.Format("м.Харків, вул. {0} {1}",
                AddressSamples[_randomizer.Next(0, AddressSamples.Length - 1)],
                _randomizer.Next(1, 250));
        }
        internal string GetRandomTelephone()
        {
            return string.Format("+380{0}", _randomizer.Next(100000000, 999999999));
        }
        internal string GetRandomFirmName()
        {
            return String.Format("{0}{1}",
               FirmsTitle[_randomizer.Next(0, FirmsTitle.Length - 1)],
               _randomizer.Next(1, 5000));
        }
        internal DateTime GetRandomDate()
        {
            return new DateTime(
                _randomizer.Next(1950, 1995),
                _randomizer.Next(1, 10),
                _randomizer.Next(1, 25));
        }



        internal double GetRandomInsPrice(string desc)
        {
            if (desc.Equals("Дешевий(а)"))
                return _randomizer.NextDouble() * _randomizer.Next(7000, 40000);
            else
                return _randomizer.NextDouble() * _randomizer.Next(50000, 100000);
        }

        public List<string> GetPrivateObjectNames()
        {

            List<string> objNames = new List<string>();
            objNames.AddRange(PrivareObjType);
            return objNames;

        }

        internal List<string> GetAutoRisks()
        {
            List<string> risks = new List<string>();

            risks.AddRange(AutoRisc);

            return risks;
        }

        internal List<string> GetPrivateRisks()
        {
            List<string> risks = new List<string>();

            risks.AddRange(ObjectRisk);

            return risks;
        }

        internal List<string> GetLifeRisks()
        {
            List<string> risks = new List<string>();

            risks.AddRange(LifeRisc);

            return risks;
        }

        public List<string> GetAutoObjectNames()
        {

            List<string> objNames = new List<string>();
            objNames.AddRange(AutoInsObjectTyps);
            return objNames;

        }
        public List<string> GetLifeObjectNames()
        {

            List<string> objNames = new List<string>();
            objNames.AddRange(LifeInsObjectTyps);
            return objNames;

        }

    }
}
