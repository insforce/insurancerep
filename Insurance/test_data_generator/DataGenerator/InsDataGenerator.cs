﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;
using test_data_generator.DataDictionary;
using System.Data.Entity;

namespace test_data_generator.DataGenerator
{
   
    public class InsDataGenerator
    {
        #region MyRegion
        private Random _randomizer;
        private RandomDataProvider _dp;

        public InsDataGenerator(RandomDataProvider dp)
        {
            _dp = dp;
            _randomizer = new Random();
        }

        //Генерирует агентов
        public Agent GenerateAgent()
        {
            Agent agent = new Agent();

            agent.Address = _dp.GetRandomAddress();
            agent.FIO = _dp.GetRandomFio();
            agent.BranchId = _randomizer.Next(1, 4);
            agent.Telephone = _dp.GetRandomTelephone();

            return agent;
        }

        //Генерирует покупателя
        public Customer GenerateCustomer()
        {
            Customer customer = new Customer();
            customer.ActualAddress = _dp.GetRandomAddress();
            customer.LegalAddress = customer.ActualAddress;
            customer.CustomerTypeId = _randomizer.Next(1, 3);
            if (customer.CustomerTypeId == 1)
            {
                customer.Name = _dp.GetRandomFirmName();
                customer.LegalPerson = GenerateLegalPerson();
            }
            else
            {
                customer.Name = _dp.GetRandomFio();
                customer.PhysicalPerson = GeneratePhysPerson();
            }
            return customer;
        }

        //Генерирует директора отдела
        public Director GenerateDirector(int id)
        {
            Director director = new Director();
            director.BranchId = id;
            director.FIO = _dp.GetRandomFio();
            director.Login = "dir" + director.BranchId;
            director.Password = director.Login;

            return director;
        }

        //Генерирует юр.лицо
        public LegalPerson GenerateLegalPerson()
        {
            LegalPerson legperson = new LegalPerson();
            legperson.CodeEDRPOU = _randomizer.Next(10000000, 99999999).ToString();
            legperson.PaymentDetails = _randomizer.Next(100000000, 999999999).ToString();
            legperson.CertificateBusiness = _dp.GetRandomCertificateBusiness();
            return legperson;
        }

        //Генерирует паспортные данные
        public Passport GeneratePassportData()
        {
            Passport pasport = new Passport();
            pasport.DataReceipt = _dp.GetRandomDate();
            pasport.PassportSeries = "МТ";
            pasport.PassportNumber = _randomizer.Next(100000, 999999);
            pasport.Issued = "Паспортный стол";

            return pasport;

        }

        //Генерирует физ.лицо
        public PhysicalPerson GeneratePhysPerson()
        {
            PhysicalPerson physperson = new PhysicalPerson();
            physperson.DriverNumber = _randomizer.Next(100000, 999999);
            physperson.DriverSeries = "АХА";
            physperson.IdentificationCode = _randomizer.Next(100000000, 999999999);
            physperson.Passport = GeneratePassportData();
            return physperson;
        }

        // генерирует информацию для авторизации агента
        public LoginData GenetateLoginData(int id)
        {
            LoginData logindata = new LoginData();
            logindata.AgentId = id;
            logindata.Login = "agent" + id;
            logindata.Password = logindata.Login;

            return logindata;
        }


        public Contract GetRandomContract (DataContext context)
        {
            context.InsuranceObjectType.Load();
            

            Contract contract = new Contract();
            contract.AgentId = _randomizer.Next(1,15);
            contract.ContractNumber = _randomizer.Next(10000000, 99999999);
            contract.ContractSeries = "ИС";
            contract.CustomerId = _randomizer.Next(1,31);
            contract.DateRegistration = _dp.GetRandomDate();
            contract.DateStart = contract.DateRegistration.AddDays(2);
            contract.DateFinish = contract.DateStart.AddMonths(6);


            int insTypeId = _randomizer.Next(1, 4);
            var insObjTypes = context.InsuranceObjectType.Where(a => a.TypeInsuranceId == insTypeId).ToList();
            int randObjtypeid = insObjTypes[_randomizer.Next(0, insObjTypes.Count)].InsuranceObjectTypeId;
    
            InsuranceObject insObject = new InsuranceObject();
            insObject.Description = _dp.GetRandomDesc();
            insObject.InsuranceObjectTypeId = randObjtypeid;

            contract.InsuranceObject = insObject;


            context.Risk.Load();

            var ourrisks = context.Risk.Local.Where(r=> r.TypeInsuranceId == insTypeId).ToList();
            contract.Risk = ourrisks;

            contract.InsuredSum = _randomizer.Next(100000, 200000);

            return contract;

        }
        
        #endregion







        public List<Risk> GetRisks()
        {
            List<Risk> riskList = new List<Risk>();

            var risknames = _dp.GetLifeRisks();
            foreach (var item in risknames)
            {
                Risk newRisk = new Risk();
                newRisk.Name = item;
                newRisk.TypeInsuranceId = 1;
                newRisk.RiscPrice = _randomizer.Next(1000,5000);
                riskList.Add(newRisk);
            }

            risknames = _dp.GetPrivateRisks();
            foreach (var item in risknames)
            {
                Risk newRisk = new Risk();
                newRisk.Name = item;
                newRisk.TypeInsuranceId = 2;
                newRisk.RiscPrice = _randomizer.Next(1000, 5000);
                riskList.Add(newRisk);
            }

            risknames = _dp.GetAutoRisks();
            foreach (var item in risknames)
            {
                Risk newRisk = new Risk();
                newRisk.Name = item;
                newRisk.TypeInsuranceId = 3;
                newRisk.RiscPrice = _randomizer.Next(1000, 5000);
                riskList.Add(newRisk);
            }

            return riskList;
        }


        public List<Branch> GetBranches()
        {
            List<Branch> branches = new List<Branch>()
            {
                new Branch() { Address = "м.Харків, вул. Пушкінська 13", Name="Головне відділення", Telephone="+380576666666" },
                new Branch() { Address = "м.Харків, вул. Динамо 42", Name="Відділення на Динамо", Telephone="+380576666665"  },
                new Branch() { Address = "м.Харків, вул. Сумська 100", Name="Відділення на Сумській", Telephone="+380576666664"  }
            };

            return branches;
        }

        public List<TypeInsurance> GetInsTypes()
        {
            List<TypeInsurance> instypes = new List<TypeInsurance>()
            {
                new TypeInsurance() { Name = "Страхування життя", TariffRate=12 },
                new TypeInsurance() { Name = "Страхування майна", TariffRate=13 },
                new TypeInsurance() { Name = "Страхування авто", TariffRate=14 }
            };
            return instypes;
        }

        public List<InsuranceObjectType> GetInsObjectTypes()
        {
            List<InsuranceObjectType> objTypes = new List<InsuranceObjectType>();
            var objTypesNames = _dp.GetLifeObjectNames();
            
            foreach (var item in objTypesNames)
            {
                InsuranceObjectType newType = new InsuranceObjectType();
                newType.Name = item;
                newType.ObjectTypePrice = _randomizer.Next(100,500);
                newType.TypeInsuranceId = 1;
                objTypes.Add(newType);

            }

            objTypesNames = _dp.GetAutoObjectNames();
            foreach (var item in objTypesNames)
            {
                InsuranceObjectType newType = new InsuranceObjectType();
                newType.Name = item;
                newType.ObjectTypePrice = _randomizer.Next(100, 500);
                newType.TypeInsuranceId = 3;
                objTypes.Add(newType);
            }

            objTypesNames = _dp.GetPrivateObjectNames();
            foreach (var item in objTypesNames)
            {
                InsuranceObjectType newType = new InsuranceObjectType();
                newType.Name = item;
                newType.ObjectTypePrice = _randomizer.Next(100, 500);
                newType.TypeInsuranceId = 2;
                objTypes.Add(newType);

            }

            return objTypes;

        }

        public List<CustomerType> GetCustomerTypes()
        {
            List<CustomerType> custype = new List<CustomerType>()
            {
                new CustomerType() { Name="Юридическое лицо" },
                new CustomerType() { Name="Физическое лицо" }
            };
            return custype;
        }





    }
}
