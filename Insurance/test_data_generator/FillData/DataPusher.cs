﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;
using test_data_generator.DataGenerator;

namespace test_data_generator.FillData
{
    public class DataPusher : IDataPusher
    {

        InsDataGenerator _datagenerator;

        public DataPusher(InsDataGenerator datagenerator)
        {
            _datagenerator = datagenerator;
        }

        public void FillDb(DataContext context)
        {


            #region Fill static data

            _datagenerator.GetBranches().ForEach(b => context.Branch.Add(b));
            context.SaveChanges();

            _datagenerator.GetInsTypes().ForEach(a => context.TypeInsurance.Add(a));
            context.SaveChanges();

            _datagenerator.GetCustomerTypes().ForEach(a => context.CustomerType.Add(a));
            context.SaveChanges();

            _datagenerator.GetRisks().ForEach(a => context.Risk.Add(a));
            context.SaveChanges();

            _datagenerator.GetInsObjectTypes().ForEach(a => context.InsuranceObjectType.Add(a));
            context.SaveChanges();

            #endregion

            for (int i = 1; i < 4; i++)
                context.Director.Add(_datagenerator.GenerateDirector(i));
            context.SaveChanges();

            for (int i = 1; i < 15; i++)
                context.Agent.Add(_datagenerator.GenerateAgent());
            context.SaveChanges();

            for (int i = 1; i < 15; i++)
                context.LoginData.Add(_datagenerator.GenetateLoginData(i));
            context.SaveChanges();

            for (int i = 1; i < 31; i++)
                context.Customer.Add(_datagenerator.GenerateCustomer());
            context.SaveChanges();


            for (int i = 1; i < 101; i++)
            {
                context.Contract.Add(_datagenerator.GetRandomContract(context));
            }
            context.SaveChanges();


        }
    }
}
