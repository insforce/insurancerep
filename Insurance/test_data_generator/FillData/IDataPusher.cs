﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;

namespace test_data_generator.FillData
{
    public interface IDataPusher
    {
        void FillDb(DataContext context);
    }
}
