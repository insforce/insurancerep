﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test_data_generator.DataAccess;
using test_data_generator.DataDictionary;
using test_data_generator.DataGenerator;
using test_data_generator.FillData;

namespace test_data_generator
{
    class Program
    {
        private static void WriteExeptionInfo(Exception e)
        {
            if (e == null)
                return;
            Console.WriteLine(e.Message);
            WriteExeptionInfo(e.InnerException);
        }

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Lets start...");
                DataContext context = new DataContext();
                RandomDataProvider rdp = new RandomDataProvider();
                InsDataGenerator datagenerator = new InsDataGenerator(rdp);
                DataPusher dataPusher = new DataPusher(datagenerator);
                Console.WriteLine("Generating data...");
                dataPusher.FillDb(context);
                Console.WriteLine("Data added :^)");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("Ohh Noooo!!!");
                WriteExeptionInfo(e);
            }
        }
    }
}
